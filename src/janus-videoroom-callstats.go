package src

import (
	"html/template"
	"net/http"

	"log"
)

var tpl *template.Template

func init() {
	var err error
	tpl, err = template.New("").Delims("[[", "]]").ParseGlob("templates/*.html")
	//tpl, err = template.New("").ParseGlob("templates/*.html")
	if err != nil {
		log.Fatal(err)
	}
	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/navbar.html", navbarHandler)
	http.HandleFunc("/footer.html", footerHandler)
	http.Handle("/favicon.ico", http.NotFoundHandler())
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	if origin := r.Header.Get("Origin"); origin != "" {
		w.Header().Set("Access-Control-Allow-Origin", "*")
	}
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")

	err := tpl.ExecuteTemplate(w, "index.html", nil)
	if err != nil {
		log.Fatalln(err)
	}
}
func navbarHandler(w http.ResponseWriter, r *http.Request) {
	err := tpl.ExecuteTemplate(w, "navbar.html", nil)
	if err != nil {
		log.Fatalln(err)
	}
}

func footerHandler(w http.ResponseWriter, r *http.Request) {
	err := tpl.ExecuteTemplate(w, "footer.html", nil)
	if err != nil {
		log.Fatalln(err)
	}
}
