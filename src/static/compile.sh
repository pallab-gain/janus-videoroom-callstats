#!/usr/bin/env bash

# Helper script to
# 1. Move Janus SDK to static container, and do necessary compilation if necessary

OUT_DIR=`pwd`
function generateSDK(){
    fromjs=${OUT_DIR}/library/janus.js
    tojs=${OUT_DIR}/dist/js/janus.js

    cp -f ${fromjs} ${tojs}
}

generateSDK
#$1